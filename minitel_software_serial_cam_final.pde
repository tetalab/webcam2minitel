#include <NewSoftSerial.h>
NewSoftSerial mySerial(6, 7);

void setup() {

  Serial.begin(1200); // port serie vers le PC
  mySerial.begin(1200); // port serie vers le minitel
  mySerial.print(12,BYTE); //efface l'écran
  serialprint7(0x0E); // passe en mode graphique
}

void serialprint7(byte b) // permet d'ecrire en 7 bits + parité sur le software serial
{
  boolean  i = false;
  for(int j = 0; j<8;j++)
  {
    if (bitRead(b,j)==1) i =!i; //calcul de la parité
  }
  if (i) bitWrite(b,7,1); //ecriture de la partié
  else bitWrite(b,7,0);//ecriture de la partié
  mySerial.print(b);//ecriture du byte sur le software serial
}

void loop() //tout ce que je recois sur le port serie, je le renvoi sur le software serial
{
  byte b =255;
  while (b == 255)
  {
    b = Serial.read();
  }
  serialprint7(b);
} 
